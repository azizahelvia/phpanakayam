 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Document</title>
     <style>
        *{
            padding: 0;
            margin: 0;
            font-family: sans-serif, "montserrat";
        }
        body{
            background color:#4834d4;
            text-align: center;
            display: flex;
            align-items: center;
            flex-direction: column;
            height: 100vh;
            width: 100vw;
            background-color: #54a0ff;
            justify-content: center;
        }
        .container{
            height: 200px;
            width: 250px;
            background-color: #4834d4;
            border:solid #1e3799 2px;
            border-radius: 10px;
        }
        p{
            font-size: 16px;
            padding-bottom: 10px;
            color: white;
        }
        input{
            width: 200px;
            height: 25px;
            border: solid #22a6b3 1px;
            border-radius: 15px;
        }
        .btn{
            margin-top: 5px;
            color: #22a6b3;
            transition: 0.5s;
        }
        .btn:hover{
            background-color: #2980b9;
            color: white;3
        }
        .siswa h2{
            text-align: center;
            letter-spacing: 0.1em;   
            font-size: 40px;      
            background: linear-gradient(to right, #eb2f06, #e58e26);
            -webkit-text-fill-color: transparent;
            -webkit-background-clip: text;
        }
        .siswa h3{
            text-align: center;
            letter-spacing: 0.1em;
            font-size: 35px;         
            background: linear-gradient(to right, #ff4757, #e58e26);
            -webkit-text-fill-color: transparent;
            -webkit-background-clip: text;
        }
        .siswa h4{
            text-align: center;
            letter-spacing: 0.1em;   
            font-size: 30px;      
            background: linear-gradient(to right, #5f27cd, #e58e26);
            -webkit-text-fill-color: transparent;
            -webkit-background-clip: text;
        }
     </style>
 </head>
 <body>
    <div class="siswa">
        <h2>Azizah Elvia</h2>
        <h3> X-RPL</h3>
        <h4> Absen: 3</h4></br>
    </div>    
   <div class="container"> 
     <form method="POST" action="looping.php" style="text-align: center;">
        <p>Berapa jumlah anak ayam awalnya?</p>
        <input type="number" name="anak_ayam" class="num"></br></br>
        <input type="submit" value="Hitung" class="btn">
    </form>  
</div>
 </body>
 </html> 